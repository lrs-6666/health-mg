import axios from 'axios'

axios.defaults.timeout = 50000; //响应时间
// 服务器
 const baseURL = "http://10.50.2.102:18081/apiI"; //线上
//  const baseURL = "http://113.24.203.53:5432/apiI"; //线上
axios.defaults.baseURL = baseURL

//POST传参序列化(添加请求拦截器)
axios.interceptors.request.use((config) => {
    //在发送请求之前做某件事
    //携带请求头
    let token = window.localStorage.getItem("accessToken")
    config.headers['Content-Type'] = 'application/json'
    config.headers['X-Access-Token'] = token
    return config;
}, (error) => { 
    //     console.log('错误的传参')
    return Promise.reject(error);
});

//返回状态判断(添加响应拦截器)
axios.interceptors.response.use((res) => {
    //对响应数据做些事
    if (!res.data.success) {
        return Promise.resolve(res);
    }
    return res;
}, (error) => {
    console.log('网络异常')
    return Promise.reject(error);
});

//返回一个Promise(发送post请求)
export function fetchPost(url, params) {
    return new Promise((resolve, reject) => {
        axios.post(url, params)
            .then(response => {
                resolve(response.data);
            }, err => {
                reject(err);
            })
            .catch((error) => {
                reject(error)
            })
    })
}
// 返回一个Promise(发送get请求)
export function fetchGet(url, param) {
    return new Promise((resolve, reject) => {
        axios.get(url, {
                params: param
            })
            .then(response => {
                resolve(response.data)
            }, err => {
                reject(err)
            })
            .catch((error) => {
                reject(error)
            })
    })
}
export default {
    fetchPost,
    fetchGet,
}