import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import moment from "moment";
import ElementUI from "element-ui";
import "element-ui/lib/theme-chalk/index.css";
import "./css/font.css";
import * as echarts from "echarts";

Vue.use(ElementUI);
moment.locale("zh-cn");
Vue.prototype.$Moment = moment;
Vue.prototype.$echarts = echarts;

new Vue({
  router,
  render: (h) => h(App),
}).$mount("#app");
