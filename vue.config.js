module.exports = {
  publicPath: './',
  outputDir: 'healthMg',
  chainWebpack: config => {
    config.module
      .rule('less')
      .test(/\.less$/)
      .oneOf('vue')
      .use('px2rem-loader')
      .loader('px2rem-loader')
      .before('postcss-loader') // this makes it work.
      .options({
        // remUnit: 75,
        // remPrecision: 8
        remUnit: 54, //根据视觉稿，rem为px的十分之一，1920px  192 rem
        remPrecision: 8 //换算的rem保留几位小数点
      })
      .end()
  }
}